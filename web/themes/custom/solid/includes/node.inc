<?php
/**
 * @file
 * Node preprocesses.
 */

/**
 * Implements hook_preprocess_node().
 */
function solid_preprocess_node(array &$variables) {
  // Default to turning off byline/submitted.
  //$variables['display_submitted'] = FALSE;
}
